package main

import (
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"sync"
	"time"
)

const (
	collectorResolution    = 24 * time.Hour
	collectorTempExtension = ".tmp"
)

type bridgeTestResult struct {
	Functional      bool
	FingerprintHash string
}

// CollectorMetrics stores the metrics to produce a metrics document for CollecTor
type CollectorMetrics struct {
	logger   *log.Logger
	filename string

	CachedRequests uint
	BridgeTests    []bridgeTestResult
	LastPublished  time.Time

	//synchronization for access to bridgestrap collector
	lock sync.Mutex
}

var collectorMetrics *CollectorMetrics

// InitCollectorMetrics initializes the global variable collectorMetrics
// it must be called once before the variable is being used
func InitCollectorMetrics(metricsFilename string) error {
	collectorMetrics = new(CollectorMetrics)
	err := loadCollectorMetrics(metricsFilename + collectorTempExtension)
	if err != nil {
		return err
	}
	if collectorMetrics.LastPublished.IsZero() {
		collectorMetrics.LastPublished = time.Now()
	}

	metricsFile, err := os.OpenFile(metricsFilename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	collectorMetrics.logger = log.New(metricsFile, "", 0)
	collectorMetrics.filename = metricsFilename

	go collectorMetrics.logMetrics()
	return nil
}

func loadCollectorMetrics(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}
		return err
	}
	defer f.Close()

	d := json.NewDecoder(f)
	return d.Decode(collectorMetrics)
}

// IncCachedRequests increments the counter of requests that have being answered by a cached result
func (m *CollectorMetrics) IncCacheRequests() {
	m.lock.Lock()
	defer m.lock.Unlock()

	m.CachedRequests++
}

// AddBridgeTest adds the functional status of the bridge to the metrics
// fingerprint can be an empty string, and the fingerprint will be retrieved if possible from the bridgeLine
func (m *CollectorMetrics) AddBridgeTest(functional bool, fingerprint string, bridgeLine string) {
	test := bridgeTestResult{
		Functional: functional,
	}

	fp := ""
	if fingerprint != "" {
		fp = fingerprint
	} else if result := Fingerprint.Find([]byte(bridgeLine)); len(result) != 0 {
		fp = string(result)
	}
	if fp != "" {
		var err error
		test.FingerprintHash, err = hashFingerprint(fp)
		if err != nil {
			log.Println("Error hashing fingerprint", fp, err)
		}
	} else if functional {
		log.Println("The bridge was functional but we couldn't get it's fingerprint:", bridgeLine)
	}

	m.lock.Lock()
	defer m.lock.Unlock()
	m.BridgeTests = append(m.BridgeTests, test)
}

func (m *CollectorMetrics) Save() {
	m.lock.Lock()
	defer m.lock.Unlock()

	filename := m.filename + collectorTempExtension
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		log.Println("Error opening the collector temp file", filename, ":", err)
		return
	}
	defer f.Close()

	d := json.NewEncoder(f)
	err = d.Encode(collectorMetrics)
	if err != nil {
		log.Println("Error encoding temp collector metrics:", err)
	}
}

// logMetrics in intervals specified by collectorResolution
func (m *CollectorMetrics) logMetrics() {
	heartbeat := time.Tick(15 * time.Minute)
	for range heartbeat {
		if m.LastPublished.Add(collectorResolution).Before(time.Now()) {
			m.publishMetrics()
			m.Save()
		}
	}
}

func (m *CollectorMetrics) publishMetrics() {
	m.lock.Lock()
	defer m.lock.Unlock()

	log.Println("Publishing CollecTor metrics")

	m.logger.Println("bridgestrap-stats-end", time.Now().UTC().Format("2006-01-02 15:04:05"), fmt.Sprintf("(%d s)", int(collectorResolution.Seconds())))
	m.logger.Println("bridgestrap-cached-requests", m.CachedRequests)

	sort.Slice(m.BridgeTests, func(i, j int) bool {
		return m.BridgeTests[i].FingerprintHash < m.BridgeTests[j].FingerprintHash
	})
	for _, test := range m.BridgeTests {
		m.logger.Println("bridgestrap-test", test.Functional, test.FingerprintHash)
	}

	m.CachedRequests = 0
	m.BridgeTests = nil
	m.LastPublished = time.Now()
}

// Handler is a http handler that responds with all the past stored metrics
func (m *CollectorMetrics) Handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")

	metricsFile, err := os.OpenFile(m.filename, os.O_RDONLY, 0644)
	if err != nil {
		log.Println("Error opening", m.filename, "file for reading:", err)
		http.NotFound(w, r)
		return
	}

	if _, err := io.Copy(w, metricsFile); err != nil {
		log.Printf("copying metricsFile returned error: %v", err)
	}
}

func hashFingerprint(fp string) (string, error) {
	fpBytes, err := hex.DecodeString(fp)
	if err != nil {
		return "", err
	}

	hasher := sha1.New()
	hasher.Write(fpBytes)
	hashedFp := hex.EncodeToString(hasher.Sum(nil))
	hashedFp = strings.ToUpper(hashedFp)
	return hashedFp, nil
}
